#include "stdafx.h"
#include "BasicDemo.h"
#include "BasicDemoDlg.h"
#include <string.h> 
#include <comdef.h>
#include <gdiplus.h>

#include <HalconCpp.h>
#include <HalconCDefs.h>
#include <HOperatorSetLegacy.h>
using namespace HalconCpp;

using namespace Gdiplus;
#pragma comment( lib, "gdiplus.lib" ) 
#include <WinGDI.h> 


class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);  

protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

CBasicDemoDlg::CBasicDemoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBasicDemoDlg::IDD, pParent)
    , m_bConnect(0)
    , m_bStartJob(0)
    ,m_pcDataBuf(NULL)
    ,m_bBitmapInfo(NULL)
{
    memset(&m_stDeviceInfoList, 0, sizeof(m_stDeviceInfoList));
	m_nDevNum = 0;

    m_handle = NULL;
    m_hWndDisplay = NULL;

    m_MaxImageSize = 0;
    m_pcDataBuf = NULL;

    hProcessThread = NULL;	

    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CBasicDemoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_OPEN_BUTTON, m_ctrlOpenButton);
	DDX_Control(pDX, IDC_CLOSE_BUTTON, m_ctrlCloseButton);
	DDX_Control(pDX, IDC_START_GRABBING_BUTTON, m_ctrlStartGrabbingButton);
	DDX_Control(pDX, IDC_STOP_GRABBING_BUTTON, m_ctrlStopGrabbingButton);
	DDX_Control(pDX, IDC_EXPOSURE_EDIT, m_ctrlExposureEdit);
	DDX_Control(pDX, IDC_GAIN_EDIT, m_ctrlGainEdit);
	DDX_Control(pDX, IDC_FRAME_RATE_EDIT, m_ctrlFrameRateEdit);
	DDX_Control(pDX, IDC_GET_PARAMETER_BUTTON, m_ctrlGetParameterButton);
	DDX_Control(pDX, IDC_SET_PARAMETER_BUTTON, m_ctrlSetParameterButton);
	DDX_Control(pDX, IDC_DEVICE_COMBO, m_ctrlDeviceCombo);
	DDX_Control(pDX, IDC_RADIO1, m_ctrlRadio1);
	DDX_Control(pDX, IDC_RADIO2, m_ctrlRadio2);
}

BEGIN_MESSAGE_MAP(CBasicDemoDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
    ON_WM_MOUSEWHEEL()
    ON_WM_MOUSEMOVE()
    ON_WM_LBUTTONDOWN()
    ON_WM_LBUTTONUP()
    ON_BN_CLICKED(IDC_ENUM_BUTTON, &CBasicDemoDlg::OnBnClickedEnumButton)
    ON_BN_CLICKED(IDC_OPEN_BUTTON, &CBasicDemoDlg::OnBnClickedOpenButton)
    ON_BN_CLICKED(IDC_START_GRABBING_BUTTON, &CBasicDemoDlg::OnBnClickedStartGrabbingButton)
    ON_BN_CLICKED(IDC_BTN_Start, &CBasicDemoDlg::OnBnClickedStartGrabbingButton)
    ON_BN_CLICKED(IDC_GET_PARAMETER_BUTTON, &CBasicDemoDlg::OnBnClickedGetParameterButton)
    ON_BN_CLICKED(IDC_SET_PARAMETER_BUTTON, &CBasicDemoDlg::OnBnClickedSetParameterButton)
    ON_BN_CLICKED(IDC_STOP_GRABBING_BUTTON, &CBasicDemoDlg::OnBnClickedStopGrabbingButton)
    ON_BN_CLICKED(IDC_CLOSE_BUTTON, &CBasicDemoDlg::OnBnClickedCloseButton)
    ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_SAVE_TIFF_BTN, &CBasicDemoDlg::OnBnClickedSaveTiffBtn)
	ON_BN_CLICKED(IDC_SAVE_BMP_BTN, &CBasicDemoDlg::OnBnClickedSaveBmpBtn)
	ON_BN_CLICKED(IDC_SAVE_JPG_BTN, &CBasicDemoDlg::OnBnClickedSaveJpgBtn)
	ON_BN_CLICKED(IDC_SAVE_RAW_BTN, &CBasicDemoDlg::OnBnClickedSaveRawBtn)
	ON_BN_CLICKED(IDC_RADIO1, &CBasicDemoDlg::OnBnClickedRadio1)
	ON_BN_CLICKED(IDC_RADIO2, &CBasicDemoDlg::OnBnClickedRadio2)
    ON_BN_CLICKED(IDC_SAVE_HALCON_SAVE, &CBasicDemoDlg::OnBnClickedSaveHalconSave)
END_MESSAGE_MAP()

BOOL CBasicDemoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

    // 创建窗口句柄
    CWnd *pWnd = GetDlgItem(IDC_DISPLAY_STATIC);
    if (NULL == pWnd)
    {
        return MV3D_RGBD_E_RESOURCE;
    }
    m_hWndDisplay = pWnd->GetSafeHwnd();
    if (NULL == m_hWndDisplay)
    {
        return MV3D_RGBD_E_RESOURCE;
    }

    GdiplusStartupInput gdiplusStartupInput;
    ULONG_PTR gdiplusToken;
    //初始化GDI+
    GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	SetCtrlWhenInit();
    return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

int CBasicDemoDlg::InitResources()
{
    int nRet = MV3D_RGBD_OK;
    CString cstrInfo;

    try
    {
        int nSensorWidth = 0;
        int nSensorHight = 0;

		MV3D_RGBD_PARAM pstValue;
		memset(&pstValue, 0, sizeof(MV3D_RGBD_PARAM));
		
		pstValue.enParamType = ParamType_Int;
        nRet = MV3D_RGBD_GetParam(m_handle, Camera_Width, &pstValue);
        if (MV3D_RGBD_OK != nRet)
        {
            cstrInfo.Format(_T("Get width failed! err code:%#x"), nRet);
            MessageBox(cstrInfo);

            nRet = MV3D_RGBD_E_UNKNOW;
            throw nRet;
        }
        nSensorWidth = pstValue.ParamInfo.stIntParam.nMax;

        memset(&pstValue, 0, sizeof(MV3D_RGBD_PARAM));
		pstValue.enParamType = ParamType_Int;
        nRet = MV3D_RGBD_GetParam(m_handle, Camera_Height, &pstValue);
        if (MV3D_RGBD_OK != nRet)
        {
            cstrInfo.Format(_T("Get hight failed! err code:%#x"), nRet);
            MessageBox(cstrInfo);
            return nRet;
        }
        nSensorHight = pstValue.ParamInfo.stIntParam.nMax;
  
        m_MaxImageSize = nSensorWidth * nSensorHight + 4096;

        CString cstrInfo;
        m_pcDataBuf =  (unsigned char*)malloc(m_MaxImageSize);
        if (NULL == m_pcDataBuf)
        {
            nRet = MV3D_RGBD_E_RESOURCE;
            throw nRet;
        }
        memset(m_pcDataBuf, 0, m_MaxImageSize);
        memset((void *)&m_stImageInfo, 0, sizeof(MV3D_RGBD_IMAGE_DATA));

    }
    catch (...)
    {
        DeInitResources();
        return nRet;
    }

    return nRet;
}

void CBasicDemoDlg::DeInitResources()
{
    if (NULL != m_pcDataBuf)
    {
        free(m_pcDataBuf);
        m_pcDataBuf = NULL;
    }
}

void CBasicDemoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}
	  
void CBasicDemoDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CBasicDemoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

BOOL CBasicDemoDlg::PreTranslateMessage(MSG* pMsg)
{
    // 屏蔽ESC和ENTER按键
    if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)
    {
        return TRUE;
    }
    if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN) 
    {
        return TRUE;
    }
    else
    {
        return CDialog::PreTranslateMessage(pMsg);
    }
}

// 渲染线程
void*  __stdcall CBasicDemoDlg::ProcessThread(void* pUser)
{
    int nRet = MV3D_RGBD_OK;

    CBasicDemoDlg* pThis = (CBasicDemoDlg*)pUser;
    if (NULL == pThis)
    {
        return NULL;
    }

	MV3D_RGBD_FRAME_DATA stFrameData = {0};
    while (pThis->m_bStartJob)
    {
        // 获取图像数据
		nRet = MV3D_RGBD_FetchFrame(pThis->m_handle, &stFrameData, 5000);
		if (MV3D_RGBD_OK ==  nRet)
		{
			try
			{
				nRet = pThis->Display(pThis->m_handle,pThis->m_hWndDisplay, stFrameData.stImageData);
                //nRet = pThis->Display(pThis->m_handle,pThis->m_hWndDisplay, &stFrameData.stImageData[1]);  //RGB图
				if (MV3D_RGBD_OK != nRet)
				{
					throw nRet;
				}
			}
            catch (...)
            {
                printf("ERROR  !\r\n");
            }
		}
        else
        {
			Sleep(1);
            continue;
        }
    }
    printf("stop recv  !\r\n");
    return NULL;
}

int  CBasicDemoDlg::Draw(MV_CODEREADER_DRAW_PARAM* pstParam)
{
    if (NULL == pstParam)
    {
        return MV3D_RGBD_E_PARAMETER;
    }   

    int nImageWidth = pstParam->nImageWidth;
    int nImageHeight = pstParam->nImageHeight;
    int nDstWidth  = (int)(pstParam->nWndRectWidth);
    int nDstHeight = (int)(pstParam->nWndRectHeight);
    int nSrcX      = 0;
    int nSrcY      = 0;
    int nSrcWidth  = (int)(nImageWidth);
    int nSrcHeight = (int)(nImageHeight);

    if (NULL == m_bBitmapInfo)
    {
        m_bBitmapInfo = (PBITMAPINFO)malloc(sizeof(BITMAPINFO) + 256 * sizeof(RGBQUAD));
        memset(m_bBitmapInfo, 0, sizeof(sizeof(BITMAPINFO) + 256 * sizeof(RGBQUAD)));
    }
    // 位图信息头
    m_bBitmapInfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);             // BITMAPINFOHEADER结构长度
    m_bBitmapInfo->bmiHeader.biWidth = nImageWidth;                         // 图像宽度
    m_bBitmapInfo->bmiHeader.biPlanes = 1;                                  // 位面数
    m_bBitmapInfo->bmiHeader.biBitCount = 8;                                // 比特数/像素的颜色深度,2^8=256
    m_bBitmapInfo->bmiHeader.biCompression = BI_RGB;                        // 图像数据压缩类型,BI_RGB表示不压缩
    m_bBitmapInfo->bmiHeader.biSizeImage = nImageWidth * nImageHeight;      // 图像大小
    m_bBitmapInfo->bmiHeader.biHeight = - nImageHeight;                     // 图像高度

    for(int i = 0; i < 256; i++)
    {
        m_bBitmapInfo->bmiColors[i].rgbBlue = m_bBitmapInfo->bmiColors[i].rgbRed = m_bBitmapInfo->bmiColors[i].rgbGreen = i;
        m_bBitmapInfo->bmiColors[i].rgbReserved = 0;
    }

    int nRet = StretchDIBits(pstParam->hDC,
        pstParam->nDstX, pstParam->nDstY, nDstWidth, nDstHeight,
        nSrcX, nSrcY, nSrcWidth, nSrcHeight, pstParam->pData, m_bBitmapInfo, DIB_RGB_COLORS, SRCCOPY);

    return MV3D_RGBD_OK;
}


int CBasicDemoDlg::Display(void* handle, void* hWnd, MV3D_RGBD_IMAGE_DATA* pstDisplayImage)
{
    int nRet = MV3D_RGBD_OK;

    if (NULL == pstDisplayImage)
    {
        return MV3D_RGBD_E_PARAMETER;
    }

    if (pstDisplayImage->nWidth == 0 || pstDisplayImage->nHeight == 0 || NULL == hWnd)
    {
        return MV3D_RGBD_E_PARAMETER;
    }

    // 显示图像
    HDC hDC  = ::GetDC((HWND)hWnd);
    SetStretchBltMode(hDC, COLORONCOLOR);
    RECT wndRect;
    ::GetClientRect((HWND)hWnd, &wndRect);

    {
        // 缓存下来，后期保存图片
        m_criSection.Lock();
        if (m_MaxImageSize <  pstDisplayImage->nDataLen)
        {
            if (NULL != m_pcDataBuf)
            {
                free(m_pcDataBuf);
                m_pcDataBuf = NULL;
            }

            m_MaxImageSize =  pstDisplayImage->nDataLen;
            m_pcDataBuf =  (unsigned char*)malloc(m_MaxImageSize);
            if (NULL == m_pcDataBuf)
            {
                nRet = MV3D_RGBD_E_RESOURCE;
                return nRet;
            }
            memset(m_pcDataBuf, 0, m_MaxImageSize);
        }

        memset(m_pcDataBuf, 0, m_MaxImageSize);
        memset((void *)&m_stImageInfo, 0, sizeof(MV3D_RGBD_IMAGE_DATA));

        // 保存图片信息 & 图片
        memcpy((void *)&m_stImageInfo, pstDisplayImage, sizeof(MV3D_RGBD_IMAGE_DATA));
        if (NULL != pstDisplayImage->pData)
        {
            memcpy(m_pcDataBuf, pstDisplayImage->pData, pstDisplayImage->nDataLen);
        }
        m_criSection.Unlock();
    }

    if (ImageType_Mono8 == pstDisplayImage->enImageType)
    {
        // mono8 直接渲染
        MV_CODEREADER_DRAW_PARAM stParam;    // 自己构建的结构体

        int nWndRectWidth  = wndRect.right  - wndRect.left;
        int nWndRectHeight = wndRect.bottom - wndRect.top;
        int nDstWidth  = (int)(nWndRectWidth);
        int nDstHeight = (int)(nWndRectHeight);
        int nDstX      = wndRect.left;
        int nDstY      = wndRect.top; 

        int nImageWidth = pstDisplayImage->nWidth;
        int nImageHeight = pstDisplayImage->nHeight;
        int nSrcX      = 0;
        int nSrcY      = 0;
        int nSrcWidth  = (int)(nImageWidth);
        int nSrcHeight = (int)(nImageHeight);

        // 给结构体赋值
        stParam.hDC = hDC;
        stParam.nDstX = nDstX;
        stParam.nDstY = nDstY;
        stParam.nImageHeight = nImageHeight;
        stParam.nImageWidth = nImageWidth;
        stParam.nWndRectHeight = nWndRectHeight;
        stParam.nWndRectWidth = nWndRectWidth;

        stParam.pData =  pstDisplayImage->pData;
        nRet = Draw(&stParam);
        if (MV3D_RGBD_OK != nRet)
        {
            return MV3D_RGBD_E_PARAMETER;
        }

    }
    else if (ImageType_Depth ==  pstDisplayImage->enImageType || ImageType_RGB8_Planar == pstDisplayImage->enImageType)
    {
        nRet = MV3D_RGBD_DisplayImage(handle, pstDisplayImage, hWnd);
        if (0 != nRet)
        {
            printf("DisplayDepthOrRgbMap failed, errcode (%#x)!\r\n",nRet);
            return nRet;
        }
    }
    else
    {
        // 不支持
    }

    ::ReleaseDC((HWND)hWnd, hDC);
    return nRet;
}

void CBasicDemoDlg::OnBnClickedEnumButton()
{
    m_ctrlDeviceCombo.ResetContent();

    int nRet = MV3D_RGBD_OK;
    CString cstrInfo;
	
	nRet = MV3D_RGBD_GetDeviceNumber(DeviceType_Ethernet | DeviceType_USB, &m_nDevNum);
	if (MV3D_RGBD_OK != nRet)
	{
		cstrInfo.Format(_T("MV3D_RGBD_GetDeviceNumber failed! err code:%#x"), nRet);
		MessageBox(cstrInfo);
		return;
	}

    // 查找设备
	memset(&m_stDeviceInfoList, 0, sizeof(m_stDeviceInfoList));

	nRet = MV3D_RGBD_GetDeviceList(DeviceType_Ethernet | DeviceType_USB,&m_stDeviceInfoList[0], 20, &m_nDevNum);  //最大20
	if (MV3D_RGBD_OK != nRet)
	{
		cstrInfo.Format(_T("MV3D_RGBD_GetDeviceList failed! err code:%#x"), nRet);
		MessageBox(cstrInfo);
		return;
	}
	
    // 显示查找到的设备信息
    for (unsigned int i = 0; i < m_nDevNum; i++)
    { 
		char  pCurrentIP[16] = ""; 
		memcpy(pCurrentIP,m_stDeviceInfoList[i].SpecialInfo.stNetInfo.chCurrentIp,strlen((const char*)m_stDeviceInfoList[i].SpecialInfo.stNetInfo.chCurrentIp));

        cstrInfo.Format(_T("[%d] %s:[%s]"), i, CStringW(m_stDeviceInfoList[i].chModelName), CStringW(pCurrentIP));
        m_ctrlDeviceCombo.AddString(cstrInfo);
    }

    m_ctrlDeviceCombo.SetCurSel(0);
    UpdateData(FALSE);

    GetDlgItem(IDC_OPEN_BUTTON)->EnableWindow(TRUE);

}

void CBasicDemoDlg::OnBnClickedOpenButton()
{
    int nRet = MV3D_RGBD_OK;
    CString cstrInfo;

    UpdateData(TRUE);

    if (true == m_bConnect)
    {
        cstrInfo.Format(_T("The camera is already connect"));
        MessageBox(cstrInfo);
        return ;
    }

    if (0 == m_nDevNum)
    {
        cstrInfo.Format(_T("Please discovery device first"));
        MessageBox(cstrInfo);
        return ;
    }

    if (m_handle)
    {
        MV3D_RGBD_CloseDevice(&m_handle);
        m_handle = NULL;
    }

    // 获取当前选择的设备信息
    int nIndex = m_ctrlDeviceCombo.GetCurSel();

    // 创建设备句柄//开启设备
    nRet = MV3D_RGBD_OpenDevice(&m_handle, &m_stDeviceInfoList[nIndex]);
    if (MV3D_RGBD_OK != nRet)
    {
        cstrInfo.Format(_T("Create handle failed! err code:%#x"), nRet);
        MessageBox(cstrInfo);
        return ;
    }

	SetWorkModeWhenOpen();

    // 获取参数
    OnBnClickedGetParameterButton();
    m_bConnect = true;

    // 初始化必要的资源
    InitResources();
	SetCtrlWhenOpen();
}

void CBasicDemoDlg::OnBnClickedCloseButton()
{
    int nRet = MV3D_RGBD_OK;
    CString cstrInfo;

    // 销毁设备句柄 
    if (NULL != m_handle)
    {
        // 停止工作流程
        if (true ==  m_bStartJob)
        {
            m_bStartJob = false;
            nRet = MV3D_RGBD_Stop(m_handle);
            if (MV3D_RGBD_OK != nRet)
            {
                cstrInfo.Format(_T("Stop grabbing failed! err code:%#x"), nRet);
                MessageBox(cstrInfo);
                m_bStartJob = false;
                return ;
            }

            //等待渲染线程完全停止
            if (NULL != hProcessThread)
            {
                // 等待线程完成，如果是多线程的，需要调用 WaitForMultipleObjects
                WaitForSingleObject(hProcessThread,INFINITE);
                // 最后关闭句柄
                CloseHandle(hProcessThread);
                hProcessThread = NULL;
            }
        }

        // 清空残留图片
        GetDlgItem(IDC_DISPLAY_STATIC)->ShowWindow(FALSE);
        GetDlgItem(IDC_DISPLAY_STATIC)->ShowWindow(TRUE);

        if (NULL != m_handle)
        {
            nRet = MV3D_RGBD_CloseDevice(&m_handle);
            if (MV3D_RGBD_OK != nRet)
            {
                cstrInfo.Format(_T("Destroy handle failed! err code:%#x"), nRet);
                MessageBox(cstrInfo);
                return ;
            }
            m_handle = NULL;
        }
    }

    //销毁资源
    DeInitResources();
	SetCtrlWhenClose();
}

void CBasicDemoDlg::OnBnClickedStartGrabbingButton()
{
    int nRet = MV3D_RGBD_OK;
    CString cstrInfo;

    if (NULL != hProcessThread)
    {
        m_bStartJob = false;
        // 等待线程完成，如果是多线程的，需要调用 WaitForMultipleObjects
        WaitForSingleObject(hProcessThread,INFINITE);
        // 最后关闭句柄
        CloseHandle(hProcessThread);
        hProcessThread = NULL;
    }
    
    m_bStartJob = true;
    // 创建接收 处理线程
    hProcessThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ProcessThread, this, 0, NULL);
    if (NULL == hProcessThread)
    {
        cstrInfo.Format(_T("Create proccess Thread failed "), 0);
        MessageBox(cstrInfo);
    }


    // 开始工作流程
    nRet = MV3D_RGBD_Start(m_handle);
    if (MV3D_RGBD_OK != nRet)
    {
        cstrInfo.Format(_T("Start grabbing failed! err code:%#x"), nRet);
        MessageBox(cstrInfo);
        return ;
    }

	SetCtrlWhenStart();
}

void CBasicDemoDlg::OnBnClickedStopGrabbingButton()
{
    int nRet = MV3D_RGBD_OK;
    CString cstrInfo;

    m_bStartJob = false;
    // 停止工作流程
    nRet = MV3D_RGBD_Stop(m_handle);
    if (MV3D_RGBD_OK != nRet)
    {
        cstrInfo.Format(_T("Stop grabbing failed! err code:%#x"), nRet);
        MessageBox(cstrInfo);
        m_bStartJob = false;
        return ;
    }
	SetCtrlWhenStop();
}

void CBasicDemoDlg::OnBnClickedGetParameterButton()
{
    int nRet = MV3D_RGBD_OK;
    CString cstrInfo;
	MV3D_RGBD_PARAM pstValue;

    // 获取曝光时间
    float fExposureTime = 0.0f;
	memset(&pstValue, 0, sizeof(MV3D_RGBD_PARAM));
	pstValue.enParamType = ParamType_Float;
    nRet = MV3D_RGBD_GetParam(m_handle, EXPOSURE_TIME, &pstValue);
    if (MV3D_RGBD_OK != nRet)
    {
        cstrInfo.Format(_T("Set exposure time failed! err code:%#x"), nRet);
        MessageBox(cstrInfo);
    }
    else
    {
        fExposureTime = pstValue.ParamInfo.stFloatParam.fCurValue;
        cstrInfo.Format(_T("%0.2f"), fExposureTime);
        m_ctrlExposureEdit.SetWindowText(cstrInfo);
    }

    // 获取增益
    float fGain= 0.0f;
    memset(&pstValue, 0, sizeof(MV3D_RGBD_PARAM));
	pstValue.enParamType = ParamType_Float;
     nRet = MV3D_RGBD_GetParam(m_handle, GAIN, &pstValue);
    if (MV3D_RGBD_OK != nRet)
    {
        cstrInfo.Format(_T("Get gain failed! err code:%#x"), nRet);
        MessageBox(cstrInfo);
    }
    else
    {
        fGain = pstValue.ParamInfo.stFloatParam.fCurValue;
        cstrInfo.Format(_T("%0.2f"), fGain);
        m_ctrlGainEdit.SetWindowText(cstrInfo);
    }

    // 获取帧率使能
    bool bFrameRateEnableBoolValue = true;
	memset(&pstValue, 0, sizeof(MV3D_RGBD_PARAM));
	pstValue.enParamType = ParamType_Bool;
     nRet = MV3D_RGBD_GetParam(m_handle, ACQUISITION_FRAME_RATE_ENABLE, &pstValue);
    if (MV3D_RGBD_OK != nRet)
    {
        cstrInfo.Format(_T("Get framerate enable failed! err code:%#x"), nRet);
        MessageBox(cstrInfo);
    }
    else
    {
		bFrameRateEnableBoolValue = pstValue.ParamInfo.bBoolParam;
		m_ctrlRadio1.SetCheck(bFrameRateEnableBoolValue ? TRUE : FALSE);
		m_ctrlRadio2.SetCheck(bFrameRateEnableBoolValue ? FALSE : TRUE);
    }

	if(m_ctrlRadio1.GetCheck() == true)
	{
		GetDlgItem(IDC_FRAME_RATE_EDIT)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_FRAME_RATE_EDIT)->EnableWindow(FALSE);
	}

    // 获取帧率
    float fFrameRate= 0.0f;
	memset(&pstValue, 0, sizeof(MV3D_RGBD_PARAM));
	pstValue.enParamType = ParamType_Float;
    nRet = MV3D_RGBD_GetParam(m_handle, ACQUISITION_FRAME_RATE, &pstValue);
    if (MV3D_RGBD_OK != nRet)
    {
        cstrInfo.Format(_T("Get acquisition rate failed! err code:%#x"), nRet);
        MessageBox(cstrInfo);
    }
    else
    {
        fFrameRate = pstValue.ParamInfo.stFloatParam.fCurValue;
        cstrInfo.Format(_T("%0.2f"), fFrameRate);
        m_ctrlFrameRateEdit.SetWindowText(cstrInfo);
    }

    UpdateData(FALSE);
}


void CBasicDemoDlg::OnBnClickedSetParameterButton()
{
    UpdateData(TRUE);
    bool bHasError = false;

    int nRet = MV3D_RGBD_OK;
    CString cstrInfo;
	MV3D_RGBD_PARAM pstValue;

    // 设置曝光时间
    float fExposureTime = 0.0f;
    m_ctrlExposureEdit.GetWindowText(cstrInfo);
    fExposureTime = atof(CStringA(cstrInfo));
	memset(&pstValue, 0, sizeof(MV3D_RGBD_PARAM));
	pstValue.enParamType = ParamType_Float;
	pstValue.ParamInfo.stFloatParam.fCurValue = fExposureTime;
    nRet = MV3D_RGBD_SetParam(m_handle, EXPOSURE_TIME, &pstValue);
    if (MV3D_RGBD_OK != nRet)
    {
        bHasError = true;
        cstrInfo.Format(_T("Set exposure time failed! err code:%#x"), nRet);
        MessageBox(cstrInfo);
    }

    // 设置增益
    float fGain= 0.0f;
    m_ctrlGainEdit.GetWindowText(cstrInfo);
    fGain = atof(CStringA(cstrInfo));
	memset(&pstValue, 0, sizeof(MV3D_RGBD_PARAM));
	pstValue.enParamType = ParamType_Float;
	pstValue.ParamInfo.stFloatParam.fCurValue = fGain;
    nRet = MV3D_RGBD_SetParam(m_handle, GAIN, &pstValue);
    if (MV3D_RGBD_OK != nRet)
    {
        bHasError = true;
        cstrInfo.Format(_T("Set gain failed! err code:%#x"), nRet);
        MessageBox(cstrInfo);
    }

	// 设置帧率使能
    float FrameRateEnableBoolValue = m_ctrlRadio1.GetCheck();
	memset(&pstValue, 0, sizeof(MV3D_RGBD_PARAM));
	pstValue.enParamType = ParamType_Bool;
	pstValue.ParamInfo.bBoolParam = FrameRateEnableBoolValue;
    nRet = MV3D_RGBD_SetParam(m_handle, ACQUISITION_FRAME_RATE_ENABLE, &pstValue);
    if (MV3D_RGBD_OK != nRet)
    {
        bHasError = true;
        cstrInfo.Format(_T("Set framerate enable failed! err code:%#x"), nRet);
        MessageBox(cstrInfo);
	}

    // 设置帧率
	if(m_ctrlRadio1.GetCheck() == true)
	{
		float fFrameRate= 0.0f;
		m_ctrlFrameRateEdit.GetWindowText(cstrInfo);
		fFrameRate = atof(CStringA(cstrInfo));
		memset(&pstValue, 0, sizeof(MV3D_RGBD_PARAM));
		pstValue.enParamType = ParamType_Float;
		pstValue.ParamInfo.stFloatParam.fCurValue = fFrameRate;
		nRet = MV3D_RGBD_SetParam(m_handle, ACQUISITION_FRAME_RATE,  &pstValue);
		if (MV3D_RGBD_OK != nRet)
		{
			bHasError = true;
			cstrInfo.Format(_T("Set acquisition rate failed! err code:%#x"), nRet);
			MessageBox(cstrInfo);
		}
	}

    if (false == bHasError)
    {
        cstrInfo.Format(_T("Set Para Success!"));
        MessageBox(cstrInfo);
    }
}

void CBasicDemoDlg::OnClose()
{
    // 关闭程序，执行断开相机、销毁句柄操作
    PostQuitMessage(0);
    CloseDevice();

    DeInitResources();
    
    CDialog::OnClose();
}

int		CBasicDemoDlg::CloseDevice(void)
{
    if (m_handle)
    {
        MV3D_RGBD_CloseDevice(&m_handle);
        m_handle = NULL;
    }

    m_bConnect = FALSE;
    m_bStartJob = FALSE;

    return MV3D_RGBD_OK;
}

void	CBasicDemoDlg::SetCtrlWhenInit()
{
	GetDlgItem(IDC_OPEN_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_CLOSE_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_START_GRABBING_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_STOP_GRABBING_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_EXPOSURE_EDIT)->EnableWindow(FALSE);
	GetDlgItem(IDC_GAIN_EDIT)->EnableWindow(FALSE);
	GetDlgItem(IDC_FRAME_RATE_EDIT)->EnableWindow(FALSE);
	GetDlgItem(IDC_GET_PARAMETER_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_SET_PARAMETER_BUTTON)->EnableWindow(FALSE);

	GetDlgItem(IDC_SAVE_RAW_BTN)->EnableWindow(FALSE);
	GetDlgItem(IDC_SAVE_TIFF_BTN)->EnableWindow(FALSE);
	GetDlgItem(IDC_SAVE_JPG_BTN)->EnableWindow(FALSE);
	GetDlgItem(IDC_SAVE_BMP_BTN)->EnableWindow(FALSE);
    GetDlgItem(IDC_SAVE_HALCON_SAVE)->EnableWindow(FALSE);


	GetDlgItem(IDC_RADIO1)->EnableWindow(FALSE);
	GetDlgItem(IDC_RADIO2)->EnableWindow(FALSE);
}

void	CBasicDemoDlg::SetCtrlWhenOpen()
{
	GetDlgItem(IDC_OPEN_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_CLOSE_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_START_GRABBING_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_EXPOSURE_EDIT)->EnableWindow(TRUE);
	GetDlgItem(IDC_GAIN_EDIT)->EnableWindow(TRUE);
	GetDlgItem(IDC_FRAME_RATE_EDIT)->EnableWindow(TRUE);
	GetDlgItem(IDC_GET_PARAMETER_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_SET_PARAMETER_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_ENUM_BUTTON)->EnableWindow(FALSE);   //不支持再次枚举
	GetDlgItem(IDC_RADIO1)->EnableWindow(TRUE);
	GetDlgItem(IDC_RADIO2)->EnableWindow(TRUE);

	GetDlgItem(IDC_SAVE_RAW_BTN)->EnableWindow(TRUE);
	GetDlgItem(IDC_SAVE_TIFF_BTN)->EnableWindow(TRUE);
	GetDlgItem(IDC_SAVE_JPG_BTN)->EnableWindow(TRUE);
	GetDlgItem(IDC_SAVE_BMP_BTN)->EnableWindow(TRUE);
    GetDlgItem(IDC_SAVE_HALCON_SAVE)->EnableWindow(TRUE);


	if(m_ctrlRadio1.GetCheck() == true)
	{
		GetDlgItem(IDC_FRAME_RATE_EDIT)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_FRAME_RATE_EDIT)->EnableWindow(FALSE);
	}
}


void	CBasicDemoDlg::SetCtrlWhenClose()
{
	// 关闭设备后清空各项参数数据
	m_ctrlExposureEdit.SetWindowText(NULL);
	m_ctrlGainEdit.SetWindowText(NULL);
	m_ctrlFrameRateEdit.SetWindowText(NULL);

	m_bStartJob = false;
	m_bConnect = false;
	GetDlgItem(IDC_OPEN_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_CLOSE_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_START_GRABBING_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_EXPOSURE_EDIT)->EnableWindow(FALSE);
	GetDlgItem(IDC_GAIN_EDIT)->EnableWindow(FALSE);
	GetDlgItem(IDC_FRAME_RATE_EDIT)->EnableWindow(FALSE);
	GetDlgItem(IDC_GET_PARAMETER_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_SET_PARAMETER_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_RADIO1)->EnableWindow(FALSE);
	GetDlgItem(IDC_RADIO2)->EnableWindow(FALSE);

	GetDlgItem(IDC_SAVE_RAW_BTN)->EnableWindow(FALSE);
	GetDlgItem(IDC_SAVE_TIFF_BTN)->EnableWindow(FALSE);
	
	GetDlgItem(IDC_SAVE_JPG_BTN)->EnableWindow(FALSE);
	GetDlgItem(IDC_SAVE_BMP_BTN)->EnableWindow(FALSE);
    GetDlgItem(IDC_SAVE_HALCON_SAVE)->EnableWindow(FALSE);

	GetDlgItem(IDC_STOP_GRABBING_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_ENUM_BUTTON)->EnableWindow(TRUE);   //支持再次枚举
}

void	CBasicDemoDlg::SetCtrlWhenStart()
{
	GetDlgItem(IDC_START_GRABBING_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_STOP_GRABBING_BUTTON)->EnableWindow(TRUE);

	GetDlgItem(IDC_SAVE_RAW_BTN)->EnableWindow(TRUE);
	GetDlgItem(IDC_SAVE_TIFF_BTN)->EnableWindow(TRUE);
	GetDlgItem(IDC_SAVE_HALCON_SAVE)->EnableWindow(TRUE);


	GetDlgItem(IDC_SAVE_JPG_BTN)->EnableWindow(TRUE);
	GetDlgItem(IDC_SAVE_BMP_BTN)->EnableWindow(TRUE);

}

void	CBasicDemoDlg::SetCtrlWhenStop()
{

	GetDlgItem(IDC_START_GRABBING_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_STOP_GRABBING_BUTTON)->EnableWindow(FALSE);

	GetDlgItem(IDC_SAVE_RAW_BTN)->EnableWindow(FALSE);
	GetDlgItem(IDC_SAVE_TIFF_BTN)->EnableWindow(FALSE);


	GetDlgItem(IDC_SAVE_HALCON_SAVE)->EnableWindow(FALSE);
	GetDlgItem(IDC_SAVE_JPG_BTN)->EnableWindow(FALSE);
	GetDlgItem(IDC_SAVE_BMP_BTN)->EnableWindow(FALSE);

}


void	CBasicDemoDlg::SetWorkModeWhenOpen()
{
	int nRet = MV3D_RGBD_OK;
	CString cstrInfo;
	bool bHasError = false;
	MV3D_RGBD_PARAM pstValue;

	// 设置工作模式
	memset(&pstValue, 0, sizeof(MV3D_RGBD_PARAM));
	pstValue.enParamType = ParamType_Enum;
	pstValue.ParamInfo.stEnumParam.nCurValue = Depth;
	nRet = MV3D_RGBD_SetParam(m_handle, WORKING_MODE, &pstValue);
	if (MV3D_RGBD_OK != nRet)
	{
		bHasError = true;
		cstrInfo.Format(_T("Set working mode failed! err code:%#x"), nRet);
		MessageBox(cstrInfo);
	}

	// 设置图片模式
	memset(&pstValue, 0, sizeof(MV3D_RGBD_PARAM));
	pstValue.enParamType = ParamType_Enum;
	pstValue.ParamInfo.stEnumParam.nCurValue = Depth_Image;
	nRet = MV3D_RGBD_SetParam(m_handle, IMG_MODE, &pstValue);
	if (MV3D_RGBD_OK != nRet)
	{
		bHasError = true;
		cstrInfo.Format(_T("Set Image mode failed! err code:%#x"), nRet);
		MessageBox(cstrInfo);
	}

	// 使能ChunkMode
	memset(&pstValue, 0, sizeof(MV3D_RGBD_PARAM));
	pstValue.enParamType = ParamType_Bool;
	pstValue.ParamInfo.bBoolParam = TRUE;
	nRet = MV3D_RGBD_SetParam(m_handle, CHUNK_MODE_ACTIVE, &pstValue);
	if (MV3D_RGBD_OK != nRet)
	{
		bHasError = true;
		cstrInfo.Format(_T("Set Chunk Mode Active failed! err code:%#x"), nRet);
		MessageBox(cstrInfo);
	}

	//设置RGB IMG
	memset(&pstValue, 0, sizeof(MV3D_RGBD_PARAM));
	pstValue.enParamType = ParamType_Enum;
	pstValue.ParamInfo.stEnumParam.nCurValue = RGB_Image;
	nRet = MV3D_RGBD_SetParam(m_handle, CHUNK_SELECTOR, &pstValue);
	if (MV3D_RGBD_OK != nRet)
	{
		bHasError = true;
		cstrInfo.Format(_T("Set Chunk Selector failed! err code:%#x"), nRet);
		MessageBox(cstrInfo);
	}

	// 使能ChunkData
	memset(&pstValue, 0, sizeof(MV3D_RGBD_PARAM));
	pstValue.enParamType = ParamType_Bool;
	pstValue.ParamInfo.bBoolParam = TRUE;
	nRet = MV3D_RGBD_SetParam(m_handle, CHUNK_ENABLE, &pstValue);
	if (MV3D_RGBD_OK != nRet)
	{
		bHasError = true;
		cstrInfo.Format(_T("Set Chunk Enable failed! err code:%#x"), nRet);
		MessageBox(cstrInfo);
	}

	//if (false == bHasError)
	//{
	//	cstrInfo.Format(_T("Set Para Success!"));
	//	MessageBox(cstrInfo);
	//}
}


int		CBasicDemoDlg::SaveImage(Mv3dRgbdFileType enFileType)
{
	int nRet = MV3D_RGBD_OK;
	CString cstrInfo;

	// 判断是否开始取流
	if (!m_bConnect)
	{
		cstrInfo.Format(_T("No camera Connected! "));
		MessageBox(cstrInfo);
		return MV3D_RGBD_E_CALLORDER;
	}

	if (!m_bStartJob)
	{
		cstrInfo.Format(_T("The camera is not startJob!"));
		MessageBox(cstrInfo);
		return MV3D_RGBD_E_CALLORDER;
	}

	// 判断是否有有效数据
	if (NULL == m_pcDataBuf)
	{
		cstrInfo.Format(_T("No data，Save Image failed!"));
		MessageBox(cstrInfo);
		return MV3D_RGBD_E_CALLORDER;
	}

	if (0 == m_stImageInfo.nDataLen)
	{
		cstrInfo.Format(_T("NO Data, save nothing !"));
		MessageBox(cstrInfo);
		return MV3D_RGBD_E_PARAMETER;
	}

	nRet = MV3D_RGBD_SaveImage(m_handle, &m_stImageInfo, enFileType, "");
	if (MV3D_RGBD_OK != nRet)
	{
		cstrInfo.Format(_T("SaveImage failed"));
		MessageBox(cstrInfo);
	}

	return nRet;
}

void	CBasicDemoDlg::OnBnClickedSaveTiffBtn()
{
	int nRet = MV3D_RGBD_OK;
	CString cstrInfo;

	nRet = SaveImage(FileType_TIFF);
	if (MV3D_RGBD_OK != nRet)
	{
		cstrInfo.Format(_T("SaveImage failed"));
		MessageBox(cstrInfo);
		return;
	}

	cstrInfo.Format(_T("Save tiff image success!"));
	MessageBox(cstrInfo);
}

void	CBasicDemoDlg::OnBnClickedSaveBmpBtn()
{
	int nRet = MV3D_RGBD_OK;
	CString cstrInfo;

	nRet = SaveImage(FileType_BMP);
	if (MV3D_RGBD_OK != nRet)
	{
		cstrInfo.Format(_T("SaveImage failed"));
		MessageBox(cstrInfo);
		return;
	}

	cstrInfo.Format(_T("Save bmp image success!"));
	MessageBox(cstrInfo);
}

void	CBasicDemoDlg::OnBnClickedSaveJpgBtn()
{
	int nRet = MV3D_RGBD_OK;
	CString cstrInfo;

	nRet = SaveImage(FileType_JPG);
	if (MV3D_RGBD_OK != nRet)
	{
		cstrInfo.Format(_T("SaveImage failed"));
		MessageBox(cstrInfo);
		return;
	}

	cstrInfo.Format(_T("Save jpg image success!"));
	MessageBox(cstrInfo);
}

void	CBasicDemoDlg::OnBnClickedSaveRawBtn()
{
	int nRet = MV3D_RGBD_OK;
	CString cstrInfo;

	// 判断是否开始取流
	if (!m_bConnect)
	{
		cstrInfo.Format(_T("No camera Connected! "));
		MessageBox(cstrInfo);
		return;
	}

	if (!m_bStartJob)
	{
		cstrInfo.Format(_T("The camera is not startJob!"));
		MessageBox(cstrInfo);
		return;
	}

	// 判断是否有有效数据
	if (NULL == m_pcDataBuf)
	{
		cstrInfo.Format(_T("No valid image data，Save RAW failed!"));
		MessageBox(cstrInfo);
		return;
	}

	if (0 == m_stImageInfo.nDataLen)
	{
		cstrInfo.Format(_T("NO Data, save nothing !"));
		MessageBox(cstrInfo);
		return;             
	}

	// 保存RAW图像
	FILE* pfile;
	char filename[256] = {0};
	CTime currTime;                                     // 获取系统时间作为保存图片文件名
	currTime = CTime::GetCurrentTime(); 
	sprintf(filename,("%.4d%.2d%.2d%.2d%.2d%.2d.raw"), currTime.GetYear(), currTime.GetMonth(),
		currTime.GetDay(), currTime.GetHour(), currTime.GetMinute(), currTime.GetSecond());
	pfile = fopen(filename,"wb");
	if(pfile == NULL)
	{
		cstrInfo.Format(_T("Open file failed!"));
		MessageBox(cstrInfo);
		return ;
	}

	{
		m_criSection.Lock();
		fwrite(m_pcDataBuf, 1, m_stImageInfo.nDataLen, pfile);
		m_criSection.Unlock();
	}

	cstrInfo.Format(_T("Save raw image success!"));
	MessageBox(cstrInfo);
	fclose (pfile);
	pfile = NULL;
}

void CBasicDemoDlg::OnBnClickedRadio1()
{
	GetDlgItem(IDC_FRAME_RATE_EDIT)->EnableWindow(TRUE);
}

void CBasicDemoDlg::OnBnClickedRadio2()
{
	GetDlgItem(IDC_FRAME_RATE_EDIT)->EnableWindow(FALSE);
}



void CBasicDemoDlg::OnBnClickedSaveHalconSave()
{
    // TODO: 在此添加控件通知处理程序代码
    int nRet = MV3D_RGBD_OK;
    CString cstrInfo;

    // 判断是否开始取流
    if (!m_bConnect)
    {
        cstrInfo.Format(_T("No camera Connected! "));
        MessageBox(cstrInfo);
        return;
    }

    if (!m_bStartJob)
    {
        cstrInfo.Format(_T("The camera is not startJob!"));
        MessageBox(cstrInfo);
        return;
    }

    // 判断是否有有效数据
    if (NULL == m_pcDataBuf)
    {
        cstrInfo.Format(_T("No data，Save Tiff failed!"));
        MessageBox(cstrInfo);
        return;
    }

    if (0 == m_stImageInfo.nDataLen)
    {
        cstrInfo.Format(_T("NO Data, save nothing !"));
        MessageBox(cstrInfo);
        return;
    }


    if (ImageType_Depth ==  m_stImageInfo.enImageType)
    {
        try
        {
            m_criSection.Lock();

            HalconCpp::HObject Hobj;

            gen_image1(&Hobj, "int2", (unsigned short)m_stImageInfo.nWidth, (unsigned short)m_stImageInfo.nHeight, (Hlong)m_pcDataBuf);

            //保存深度图
            char filename[256] = { 0 };
            sprintf(filename, ("./framenum_%d.tiff"), m_stImageInfo.nFrameNum);
            write_image(Hobj, "tiff", 0, filename);

            m_criSection.Unlock();
        }
        catch (...)
        {
            m_criSection.Unlock();
            cstrInfo.Format(_T("Save halcon image failed, please check halcon software!"));
            MessageBox(cstrInfo);
            return;
        }
    }
    else if ((ImageType_RGB8_Planar == m_stImageInfo.enImageType))
    {
        try
        {
            m_criSection.Lock();
            HalconCpp::HObject Hobj;
            gen_image3(&Hobj, "byte", (unsigned short)m_stImageInfo.nWidth, (unsigned short)m_stImageInfo.nHeight, (Hlong)m_pcDataBuf, (Hlong)(m_pcDataBuf+ m_stImageInfo.nWidth*m_stImageInfo.nHeight), (Hlong)(m_pcDataBuf+ m_stImageInfo.nWidth*m_stImageInfo.nHeight*2));


            //保存深度图
            char filename[256] = { 0 };
            sprintf(filename, ("./framenum_%d.bmp"), m_stImageInfo.nFrameNum);
            write_image(Hobj, "bmp", 0, filename);

            m_criSection.Unlock();
        }
        catch (...)
        {
            m_criSection.Unlock();
            cstrInfo.Format(_T("Save halcon image failed, please check halcon software!"));
            MessageBox(cstrInfo);
            return;
        }
    }
    else
    {
        cstrInfo.Format(_T("Not support save halcon file!"));
        MessageBox(cstrInfo);
        return;
    }


    cstrInfo.Format(_T("Save halcon image success!"));
    MessageBox(cstrInfo);
}
