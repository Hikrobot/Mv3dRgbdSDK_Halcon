﻿namespace BasicDemo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            //不在这里处理的话，  渲染点云图，直接关闭窗口会有报错
            // ch:取流标志位清零 | en:Reset flow flag bit
            if (m_bGrabbing == true)
            {
                m_bGrabbing = false;
                m_hReceiveThread.Join();
            }

            // ch:关闭设备 | en:Close Device
            Mv3dRgbdSDK.MV3D_RGBD_Stop(m_handle);
            Mv3dRgbdSDK.MV3D_RGBD_CloseDevice(ref m_handle);

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbDeviceList = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bnClose = new System.Windows.Forms.Button();
            this.bnOpen = new System.Windows.Forms.Button();
            this.bnEnum = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bnStopGrab = new System.Windows.Forms.Button();
            this.bnStartGrab = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.bnSetParam = new System.Windows.Forms.Button();
            this.bnGetParam = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbGain = new System.Windows.Forms.TextBox();
            this.tbExposure = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.SaveJpgBtn = new System.Windows.Forms.Button();
            this.SaveBmpBtn = new System.Windows.Forms.Button();
            this.SaveRawBtn = new System.Windows.Forms.Button();
            this.SaveTiffBtn = new System.Windows.Forms.Button();
            this.HalconSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbDeviceList
            // 
            this.cbDeviceList.FormattingEnabled = true;
            this.cbDeviceList.Location = new System.Drawing.Point(12, 12);
            this.cbDeviceList.Name = "cbDeviceList";
            this.cbDeviceList.Size = new System.Drawing.Size(559, 20);
            this.cbDeviceList.TabIndex = 11;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pictureBox1.Location = new System.Drawing.Point(12, 45);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(559, 390);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bnClose);
            this.groupBox1.Controls.Add(this.bnOpen);
            this.groupBox1.Controls.Add(this.bnEnum);
            this.groupBox1.Location = new System.Drawing.Point(587, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(246, 100);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "初始化";
            // 
            // bnClose
            // 
            this.bnClose.Location = new System.Drawing.Point(132, 62);
            this.bnClose.Name = "bnClose";
            this.bnClose.Size = new System.Drawing.Size(88, 23);
            this.bnClose.TabIndex = 0;
            this.bnClose.Text = "关闭设备";
            this.bnClose.UseVisualStyleBackColor = true;
            this.bnClose.Click += new System.EventHandler(this.bnClose_Click);
            // 
            // bnOpen
            // 
            this.bnOpen.Location = new System.Drawing.Point(24, 62);
            this.bnOpen.Name = "bnOpen";
            this.bnOpen.Size = new System.Drawing.Size(80, 23);
            this.bnOpen.TabIndex = 1;
            this.bnOpen.Text = "打开设备";
            this.bnOpen.UseVisualStyleBackColor = true;
            this.bnOpen.Click += new System.EventHandler(this.bnOpen_Click);
            // 
            // bnEnum
            // 
            this.bnEnum.Location = new System.Drawing.Point(24, 20);
            this.bnEnum.Name = "bnEnum";
            this.bnEnum.Size = new System.Drawing.Size(196, 23);
            this.bnEnum.TabIndex = 2;
            this.bnEnum.Text = "查找设备";
            this.bnEnum.UseVisualStyleBackColor = true;
            this.bnEnum.Click += new System.EventHandler(this.bnEnum_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bnStopGrab);
            this.groupBox2.Controls.Add(this.bnStartGrab);
            this.groupBox2.Location = new System.Drawing.Point(587, 116);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(245, 70);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "采集图像";
            // 
            // bnStopGrab
            // 
            this.bnStopGrab.Location = new System.Drawing.Point(129, 30);
            this.bnStopGrab.Name = "bnStopGrab";
            this.bnStopGrab.Size = new System.Drawing.Size(88, 23);
            this.bnStopGrab.TabIndex = 0;
            this.bnStopGrab.Text = "停止采集";
            this.bnStopGrab.UseVisualStyleBackColor = true;
            this.bnStopGrab.Click += new System.EventHandler(this.bnStopGrab_Click);
            // 
            // bnStartGrab
            // 
            this.bnStartGrab.Location = new System.Drawing.Point(24, 30);
            this.bnStartGrab.Name = "bnStartGrab";
            this.bnStartGrab.Size = new System.Drawing.Size(83, 23);
            this.bnStartGrab.TabIndex = 1;
            this.bnStartGrab.Text = "开始采集";
            this.bnStartGrab.UseVisualStyleBackColor = true;
            this.bnStartGrab.Click += new System.EventHandler(this.bnStartGrab_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox4.Controls.Add(this.bnSetParam);
            this.groupBox4.Controls.Add(this.bnGetParam);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.tbGain);
            this.groupBox4.Controls.Add(this.tbExposure);
            this.groupBox4.Location = new System.Drawing.Point(587, 319);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(245, 109);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "参数设置";
            // 
            // bnSetParam
            // 
            this.bnSetParam.Location = new System.Drawing.Point(142, 83);
            this.bnSetParam.Name = "bnSetParam";
            this.bnSetParam.Size = new System.Drawing.Size(83, 23);
            this.bnSetParam.TabIndex = 0;
            this.bnSetParam.Text = "设置参数";
            this.bnSetParam.UseVisualStyleBackColor = true;
            this.bnSetParam.Click += new System.EventHandler(this.bnSetParam_Click);
            // 
            // bnGetParam
            // 
            this.bnGetParam.Location = new System.Drawing.Point(18, 83);
            this.bnGetParam.Name = "bnGetParam";
            this.bnGetParam.Size = new System.Drawing.Size(83, 23);
            this.bnGetParam.TabIndex = 1;
            this.bnGetParam.Text = "获取参数";
            this.bnGetParam.UseVisualStyleBackColor = true;
            this.bnGetParam.Click += new System.EventHandler(this.bnGetParam_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(19, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 3;
            this.label2.Text = "增益";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(20, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "曝光";
            // 
            // tbGain
            // 
            this.tbGain.Location = new System.Drawing.Point(129, 48);
            this.tbGain.Name = "tbGain";
            this.tbGain.Size = new System.Drawing.Size(95, 21);
            this.tbGain.TabIndex = 6;
            // 
            // tbExposure
            // 
            this.tbExposure.Location = new System.Drawing.Point(129, 22);
            this.tbExposure.Name = "tbExposure";
            this.tbExposure.Size = new System.Drawing.Size(95, 21);
            this.tbExposure.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 23);
            this.label4.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 23);
            this.label3.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.HalconSave);
            this.groupBox3.Controls.Add(this.SaveJpgBtn);
            this.groupBox3.Controls.Add(this.SaveBmpBtn);
            this.groupBox3.Controls.Add(this.SaveRawBtn);
            this.groupBox3.Controls.Add(this.SaveTiffBtn);
            this.groupBox3.Location = new System.Drawing.Point(587, 192);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(245, 121);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "保存图片";
            // 
            // SaveJpgBtn
            // 
            this.SaveJpgBtn.Enabled = false;
            this.SaveJpgBtn.Location = new System.Drawing.Point(164, 22);
            this.SaveJpgBtn.Name = "SaveJpgBtn";
            this.SaveJpgBtn.Size = new System.Drawing.Size(62, 23);
            this.SaveJpgBtn.TabIndex = 6;
            this.SaveJpgBtn.Text = "保存JPG";
            this.SaveJpgBtn.UseVisualStyleBackColor = true;
            this.SaveJpgBtn.Click += new System.EventHandler(this.SaveJpgBtn_Click);
            // 
            // SaveBmpBtn
            // 
            this.SaveBmpBtn.Enabled = false;
            this.SaveBmpBtn.Location = new System.Drawing.Point(96, 23);
            this.SaveBmpBtn.Name = "SaveBmpBtn";
            this.SaveBmpBtn.Size = new System.Drawing.Size(56, 23);
            this.SaveBmpBtn.TabIndex = 5;
            this.SaveBmpBtn.Text = "保存BMP";
            this.SaveBmpBtn.UseVisualStyleBackColor = true;
            this.SaveBmpBtn.Click += new System.EventHandler(this.SaveBmpBtn_Click);
            // 
            // SaveRawBtn
            // 
            this.SaveRawBtn.Enabled = false;
            this.SaveRawBtn.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SaveRawBtn.Location = new System.Drawing.Point(18, 56);
            this.SaveRawBtn.Name = "SaveRawBtn";
            this.SaveRawBtn.Size = new System.Drawing.Size(206, 23);
            this.SaveRawBtn.TabIndex = 0;
            this.SaveRawBtn.Text = "保存RAW";
            this.SaveRawBtn.UseVisualStyleBackColor = true;
            this.SaveRawBtn.Click += new System.EventHandler(this.SaveRawBtn_Click);
            // 
            // SaveTiffBtn
            // 
            this.SaveTiffBtn.Enabled = false;
            this.SaveTiffBtn.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SaveTiffBtn.Location = new System.Drawing.Point(18, 22);
            this.SaveTiffBtn.Name = "SaveTiffBtn";
            this.SaveTiffBtn.Size = new System.Drawing.Size(71, 23);
            this.SaveTiffBtn.TabIndex = 2;
            this.SaveTiffBtn.Text = "保存TIFF";
            this.SaveTiffBtn.UseVisualStyleBackColor = true;
            this.SaveTiffBtn.Click += new System.EventHandler(this.SaveTiffBtn_Click);
            // 
            // HalconSave
            // 
            this.HalconSave.Enabled = false;
            this.HalconSave.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.HalconSave.Location = new System.Drawing.Point(19, 87);
            this.HalconSave.Name = "HalconSave";
            this.HalconSave.Size = new System.Drawing.Size(206, 23);
            this.HalconSave.TabIndex = 7;
            this.HalconSave.Text = "HalconSave";
            this.HalconSave.UseVisualStyleBackColor = true;
            this.HalconSave.Click += new System.EventHandler(this.HalconSave_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 443);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cbDeviceList);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "BasicDemo C#";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbDeviceList;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bnClose;
        private System.Windows.Forms.Button bnOpen;
        private System.Windows.Forms.Button bnEnum;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button bnStopGrab;
        private System.Windows.Forms.Button bnStartGrab;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tbGain;
        private System.Windows.Forms.TextBox tbExposure;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bnSetParam;
        private System.Windows.Forms.Button bnGetParam;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button SaveRawBtn;
        private System.Windows.Forms.Button SaveTiffBtn;
        private System.Windows.Forms.Button SaveJpgBtn;
        private System.Windows.Forms.Button SaveBmpBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button HalconSave;
    }
}

